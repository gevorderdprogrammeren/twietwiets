# README #


### What is this repository for? ###

* A program with a GUI that can be used to generate rhymes from a dataset of tweets and tweet those generated tweets.


### How do I get set up? ###

* Clone the repository, execute the program twitwipoems.py, fill in an hour of choice and follow the instructions given in the GUI.
* No further configuration needed
* All the data is inside the repos

### Who do I talk to? ###

* Mark van Dam, Boris de Jong and Joep Franssen.
