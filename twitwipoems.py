#!/usr/bin/python3

owner = "TwieTwiePoems"        # Name des Accounts, an den Fehlermeldungen gesendet werden
import my_config
import tweepy
import traceback
import time                   # To get access to the tweets to be posted on our twitter
from graphics import *         # Graphics library added
from test import *


def load_files(hour,set1,set2):
    """
    loads the files needed for the program
    """
    if len(hour) == 1:
        hour = "0"+hour
    return(rhyming(combine(set1, tweets(set2,hour))))


def login():
    """
    needed to login to the twitter account
    """
    # for info on the tweepy module, see http://tweepy.readthedocs.org/en/

    # Authentication is taken from my_config.py
    consumer_key = my_config.consumer_key
    consumer_secret = my_config.consumer_secret
    access_token = my_config.access_token
    access_token_secret = my_config.access_token_secret

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)
    return api

def make_button(win, text ,x ,y , color):
    """
    draws a button
    """
    button = Rectangle(Point(x, y), Point(x + 100, y - 40))

    text = Text(Point(x + 50, y - 20), text)
    if text.getText()    == "Load":
        button = Rectangle(Point(x, y), Point(x + 100, y - 22))
        text = Text(Point(x + 50, y - 11), text.getText())
    button.setFill(color)
    button.draw(win)
    text.draw(win)


def tweet_something(debug, output):
    """ 
    Tweets something on the twitter account
    """
    api = login()
    try:
        if debug:
            print(output)
        else:
            api.update_status(status=output)
    except:
        error_msg = traceback.format_exc().split("\n", 1)[1][-130:]
        api.send_direct_message(screen_name=owner, text=error_msg + " " + time.strftime("%H:%M:%S"))

def main():
    """ 
    Handles the GUI
    """
    clickcounter = 0
    set1, set2 = pronounce()
    win = GraphWin("TwieTwiets", 810, 600)
    win.setBackground("lightblue")
    title = Text(Point(400, 100), "TwieTwiets")
    title.setSize(25)
    title.draw(win)
    tweet_channel = Text(Point(400, 150), "Follow us at: twitter.com/twietwietpoems")
    tweet_channel.setSize(16)
    tweet_channel.draw(win)
    make_button(win, "Publish", 250, 400, "lightgreen")
    make_button(win, "New Rhyme", 360, 400, "lightgrey")
    make_button(win, "Quit", 470, 400, "IndianRed2")
    make_button(win,"Load", 620, 330,"lightgrey")
    tweet_area = Rectangle(Point(30, 180), Point(790, 280))
    tweet_area.setFill("light grey")
    tweet_area.draw(win)
    tweet = Text(Point(400, 240), "")
    tweet.setSize(16)
    tweet.draw(win)
    info = Text(Point(400,450),"")
    info.setSize(14)
    info.draw(win)
    inputbox= Entry(Point(410,320),45)
    inputbox.draw(win)
    inputbox.setText("Please fill in an hour of choice to start loading the files")
    click = win.getMouse()
    while not 360 < click.getY() < 400 or not 470 < click.getX() < 570:
        if 360 < click.getY() < 400 and 250 < click.getX() < 350:
            tweet_something(False, tweet.getText())
            info.setText("Tweet sent!")
        elif 360 < click.getY() < 400 and 360 < click.getX() < 460:
            tweet.setText(send_tweet(rhyming_set))
            clickcounter+=1
            if clickcounter > len(rhyming_set) and len(rhyming_set) < 10:
                tweet.setText("Those were all rhymes for this hour")
        elif 308 < click.getY() < 330 and 620 < click.getX() < 720:
            info.setText("Loading files..(max 1 minute)")
            rhyming_set = load_files(inputbox.getText(),set1,set2)
            info.setText("Files loaded!")
        click = win.getMouse()

if __name__ == '__main__':
    main()
