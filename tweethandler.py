#!/usr/bin/python3

import gzip
import glob
import time
import re
from collections import defaultdict
import random
import itertools


def filter_tweets(text):
    """
    removes anything in tweets that contain http:,@ or RT
    """
    filtered_text= ""
    for word in text:
        if "http:" in word or "@" in word or "RT" in word:
            text.remove(word)
        else:
            filtered_text+= "{0} ".format(word)
    return(filtered_text.rstrip())

def tweets(set2,hour):
    """
    Gathers the tweets from a gzip
    """
    tweet_list = []
    filepath = "tweets/20170401:{0}.out.gz".format(hour)
    for filename in glob.glob(filepath):       
        with gzip.open(filename, 'r') as f:
            for line in f:
                line = str(line)
                text = line.split("\"")[13]
                text_list = text.split()
                text = filter_tweets(text_list)
                try:
                    if all(x.isalpha() or x.isspace() for x in text) and 40 < len(text) < 70 and text_list[-1] in set2:
                        tweet_list.append((text,text_list[-1]))
                except:
                    pass
    return(tweet_list)


def combine(set1, list1):
    """
    combines the given set and list into 1 set with the tweet + the usefull 
    info from pronounce
    """ 
    new_set = set()
    for tweet in list1:
        for item in set1:
            if tweet[1] == item[0]:
                pronounce = "".join(item[1])
                new_set.add((tweet[0], pronounce, item[2]))
    return(new_set)


def rhyming(new_set): 
    """
    puts rhyming tweets together in 1 tuple inside the list
    """
    final = set()
    newest = set()

    for item in new_set:
        klemtoonpos = [pos for pos, char in enumerate(item[1]) if char == "~"][0]
        string = item[1][klemtoonpos + 1:]
        count = 0
        for char in string:
            count += 1
            if char == item[2]:
                klinkerpos = count - 1
        base_rhyme = string[klinkerpos:]

        rhyme_list = []

        for item in new_set:
            x = True
            for test in final:
                if item[0] in test:
                    x = False
            if x:
                klemtoonpos = [pos for pos, char in enumerate(item[1]) if char == "~"][0]
                string = item[1][klemtoonpos + 1:]
                count = 0
                for char in string:
                    count += 1
                    if char == item[2]:
                        klinkerpos = count - 1
                rhyme = string[klinkerpos:]
                if rhyme == base_rhyme:
                    rhyme_list.append(item[0])
        if len(rhyme_list) > 1:
            final.add(tuple(rhyme_list))
            pairs = itertools.combinations(rhyme_list, 2)
            pairs = list(pairs)
            for item in pairs:
                if item[0].split()[-1] != item[1].split()[-1]:
                    newest.add(item)
    return(newest)


def pronounce():
    """
    gathers how all the words in pronounce are pronounced and other info
    that is needed to determine whether words rhyme
    """
    set1 = set()
    set2 = set()
    with open('dpw_new.cd') as f:
        for line in f:
            line = str(line)
            line = line.replace("'", "~")
            text = line.split("\t")
            pronounciation = re.split("-| ", text[1])
            list2 = [x for x in pronounciation if x.startswith("~")]
            if len(list2) > 0:
                vowels = ""
                set2.add(text[0])
                for letter in list2[-1]:
                    if letter in "aioeAIOEKLMy}u@":
                        vowels += letter
                set1.add((text[0], tuple(pronounciation), vowels))
    return (set1, set2)


def send_tweet(rhymepairs):
    """
    returns a random rhyme from the rhymepairs set
    """
    if len(rhymepairs) > 0:
        tweet = tweet = random.choice(tuple(rhymepairs))
        return("{}\n{}".format(tweet[0], tweet[1]))
    else:
        return("No rhymes found for this hour. Sorry")
